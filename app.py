from flask import Flask, render_template, redirect, request, url_for

app = Flask('zarupadev01')


if __name__ == '__main__':
    app.run()


@app.route('/')
def index():
    return render_template('index.html')


